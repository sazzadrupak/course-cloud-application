const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const { log } = require('@rama41222/node-logger');
const { dateTime } = require('./utils/time');

const config = {
  name: 'docker-compose-exercise',
  port: 8002,
  host: '0.0.0.0',
};

const app = express();
const logger = log({ console: true, file: false, label: config.name });

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send(200).send('Hello world from service 2');
});

app.get('/check', (req, res) => {
  res.status(200).json({
    remote: `Req came from ${req.client.remoteAddress}:${req.client.remotePort}`,
    local: `Req served at ${req.client.localAddress}:${req.client.localPort}`,
  });
});

app.listen(config.port, config.host, (e) => {
  if (e) {
    throw new Error('Internal Server Error');
  }
  logger.info(`${config.name} running on ${config.host}:${config.port}`);
  fs.readFile('./shared/logs.json', (err, data) => {
    if (err) {
      fs.writeFileSync('./shared/logs.json', JSON.stringify({ service2: `BOOT ${dateTime()}` }));
    } else {
      const json = JSON.parse(data);
      json.push({ service2: `BOOT ${dateTime()}` });
      fs.writeFileSync('./shared/logs.json', JSON.stringify(json));
    }
  });
});
