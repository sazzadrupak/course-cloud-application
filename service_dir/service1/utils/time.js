const dateTime = () => {
  const dateOb = new Date();
  const date = (`0${dateOb.getDate()}`).slice(-2);
  const month = (`0${(dateOb.getMonth() + 1)}`).slice(-2);
  const year = dateOb.getFullYear();
  const hours = dateOb.getHours();
  const minutes = dateOb.getMinutes();
  const seconds = dateOb.getSeconds();
  const formattedDateTime = `${year}-${month}-${date} ${hours}:${minutes}:${seconds}`;
  return formattedDateTime;
};

module.exports.dateTime = dateTime;
