const fibonacciSeries = (n) => {
  if (n === 1) {
    return [0, 1];
  }
  const s = fibonacciSeries(n - 1);
  s.push(s[s.length - 1] + s[s.length - 2]);
  return s;
};

module.exports.fibonacciSeries = fibonacciSeries;
