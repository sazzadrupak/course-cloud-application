const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const Docker = require('dockerode');
const fs = require('fs');
const request = require('request');
const { log, ExpressAPILogMiddleware } = require('@rama41222/node-logger');
const { fibonacciSeries } = require('./utils/fibo');
const { dateTime } = require('./utils/time');

const socket = process.env.DOCKER_SOCKET || '/var/run/docker.sock';
const docker = new Docker(socket);
const config = {
  name: 'docker-compose-exercise',
  port: 8001,
  host: '0.0.0.0',
};

const app = express();
const logger = log({ console: true, file: false, label: config.name });

app.use(bodyParser.json());
app.use(cors());
app.use(ExpressAPILogMiddleware(logger, { request: true }));

app.get('/', (req, res) => new Promise((resolve, reject) => {
  request('http://service2:8002/check', { json: true }, (err, body) => {
    if (err) reject(err);
    resolve(body);
  });
})
  .then((response) => {
    const result = {
      service1: {
        remote: `Req came from ${req.client.remoteAddress}:${req.client.remotePort}`,
        local: `Req served at ${req.client.localAddress}:${req.client.localPort}`,
      },
      service2: response.body,
    };
    const data = JSON.stringify(result, null, 2);
    fs.writeFileSync('./shared/share_file.json', data);
    res.json(result);
  })
  .catch((error) => {
    res.send(error);
  }));

app.post('/fibo', (req, res) => {
  const { number } = req.body;
  let message;
  if (!Number(number)) {
    message = 'Error: not a number';
    res.status(400).send({ error: message });
  } else if (number < 0) {
    message = 'Error: negative number';
    res.status(400).send({ error: message });
  } else {
    const fiboSeries = fibonacciSeries(number);
    res.status(200).send({ fibo: fiboSeries });
  }
});

app.get('/run-log', (req, res) => {
  fs.readFile('./shared/logs.json', (err, data) => {
    if (err) {
      throw err;
    } else {
      const objArray = JSON.parse(data);
      res.status(200).send(objArray);
    }
  });
});

app.post('/shutdown', async (req, res) => {
  docker.listContainers({ all: true }, (err, containers) => {
    if (err) {
      throw err;
    } else {
      const logObj1 = {};
      const logObj2 = {};
      logObj1.service1 = `SHUTDOWN ${dateTime()}`;
      logObj2.service2 = `SHUTDOWN ${dateTime()}`;
      fs.readFile('./shared/logs.json', (error, data) => {
        if (error) {
          fs.writeFileSync('./shared/logs.json', JSON.stringify(logObj1));
          fs.writeFileSync('./shared/logs.json', JSON.stringify(logObj2));
        } else {
          const json = JSON.parse(data);
          json.push(logObj1);
          json.push(logObj2);
          fs.writeFileSync('./shared/logs.json', JSON.stringify(json));
        }
      });
      res.status(200).send('Services are closing');

      containers.forEach((containerInfo) => {
        if (containerInfo.Names.toString() === '/service1' || containerInfo.Names.toString() === '/service2') {
          docker.getContainer(containerInfo.Id).stop('cb');
        }
      });
    }
  });
});

app.listen(config.port, config.host, (e) => {
  if (e) {
    throw new Error('Internal Server Error');
  }
  logger.info(`${config.name} running on ${config.host}:${config.port}`);
  fs.readFile('./shared/logs.json', (err, data) => {
    if (err) {
      fs.writeFileSync('./shared/logs.json', JSON.stringify({ service1: `BOOT ${dateTime()}` }));
    } else {
      const json = JSON.parse(data);
      json.push({ service1: `BOOT ${dateTime()}` });
      fs.writeFileSync('./shared/logs.json', JSON.stringify(json));
    }
  });
});
