/* eslint-env jest */
const chai = require('chai');
const supertest = require('supertest');

const { expect } = chai;
const server = supertest.agent('http://localhost:8001');

describe('Check finonacci series', () => {
  it('a positive number', async () => {
    const res = await server
      .post('/fibo')
      .send({ number: 10 });

    expect(res.statusCode).to.equal(200);
    expect(res.body).to.be.an('object');
    expect(res.body).to.have.keys(['fibo']);
    expect(res.body.fibo).to.be.an('array');
    expect(res.body.fibo).to.have.deep.members([0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55]);
  });

  it('not a number', async () => {
    const res = await server
      .post('/fibo')
      .send({ number: 'a' });

    expect(res.statusCode).to.equal(400);
    expect(res.body).to.be.an('object');
    expect(res.body).to.have.keys(['error']);
    expect(res.body.error).to.equal('Error: not a number');
  });

  it('negative number', async () => {
    const res = await server
      .post('/fibo')
      .send({ number: -1 });

    expect(res.statusCode).to.equal(400);
    expect(res.body).to.be.an('object');
    expect(res.body).to.have.keys(['error']);
    expect(res.body.error).to.equal('Error: negative number');
  });
});
