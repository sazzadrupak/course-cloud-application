## Build the project
* Open a terminal.
* Go to the project directory.
* Run the following command:
```bash
sudo docker-compose up -d
```

You will see the following output in your terminal.

```bash
Creating network "gitlab-network" with the default driver
Creating volume "cloud-app-project_gitlab-data" with default driver
Creating volume "cloud-app-project_gitlab-config" with default driver
Creating volume "cloud-app-project_gitlab-logs" with default driver
Creating volume "cloud-app-project_gitlab-runner-config" with default driver
Pulling gitlab (gitlab/gitlab-ce:latest)...
latest: Pulling from gitlab/gitlab-ce
976a760c94fc: Pull complete
c58992f3c37b: Pull complete
0ca0e5e7f12e: Pull complete
f2a274cc00ca: Pull complete
163f3071a3f8: Pull complete
d96d45e9c9e7: Pull complete
9a0f4e25d3a3: Pull complete
19aad3ea2a1d: Pull complete
fcafd8209320: Pull complete
3a4ea7fd547c: Pull complete
Digest: sha256:f5cb34c4d6bca26734dbce8889863d32c4ce0df02079b8c50bc4ac1dd89b53f4
Status: Downloaded newer image for gitlab/gitlab-ce:latest
Pulling gitlab-runner (gitlab/gitlab-runner:latest)...
latest: Pulling from gitlab/gitlab-runner
7ddbc47eeb70: Pull complete
c1bbdc448b72: Pull complete
8c3b70e39044: Pull complete
45d437916d57: Pull complete
59a312699ead: Pull complete
6562c5999ae2: Pull complete
368e9065e920: Pull complete
b92ce2befcc8: Pull complete
420f91b9ac4d: Pull complete
Digest: sha256:c40748978103959590474b81b72d58f0c240f010b4c229181aaf3132efdf4bd1
Status: Downloaded newer image for gitlab/gitlab-runner:latest
Creating gitlab        ... done
Creating gitlab-runner ... done
```
* Wait a few minutes(3/4) and then issue the following command to check the status of the gitlab-ci and gitlab-runner containers:
```bash
sudo docker ps -a
```
The output will be like this:
```bash
CONTAINER ID        IMAGE                         COMMAND                  CREATED             STATUS                   PORTS                                                              NAMES
9984267bcfc1        gitlab/gitlab-runner:latest   "/usr/bin/dumb-init …"   4 minutes ago       Up 4 minutes             0.0.0.0:8093->8093/tcp                                             gitlab-runner
a6a192a518a6        gitlab/gitlab-ce:latest       "/assets/wrapper"        4 minutes ago       Up 4 minutes (healthy)   0.0.0.0:443->443/tcp, 0.0.0.0:2224->22/tcp, 0.0.0.0:8080->80/tcp   gitlab
```
* When you see the gitlab-ci container is healthy, go to this link:
```bash
http://localhost:8080
```
* First you have to give "New password" and "Confirm new password" and click "Change your password". 
* Then it will redirect you to new window. Here first give "root" as Username and password (from previous step). After successfull registration, you will redirected to the follwing page.
![Image of Yaktocat](./successfull_login.png)
* Click "Create new project" from the page shown in previous step.
Give a project name e.g. (hello-cloud) like following.
![Image of Yaktocat](./create-project.png)
Click "Create project".
* From left side of your current gitlab-ci page, click "Settings" -> "CI / CD".
* Now click the "Expand" under "Runner" div.
* You need to disable the shared runner. Then copy the runner registration token.
![Image of Yaktocat](./runner.png)
* Paste that token into the follwing command.
```bash
sudo docker exec -it  gitlab-runner gitlab-runner register -n \
--executor="docker" \
--custom_build_dir-enabled="true" \
--docker-image="docker:dind" \
--url="http://gitlab:80" \
--clone-url="http://gitlab:80" \
--registration-token="paste-your-token-here" \
--description="docker-runner" \
--tag-list="docker-dind-runner" \
--run-untagged="true" \
--locked="false" \
--docker-tlsverify="false" \
--docker-network-mode="gitlab-network" \
--docker-disable-cache="true" \
--docker-privileged="true" \
--cache-dir="/cache" \
--builds-dir="/builds" \
--docker-volumes="gitlab-runner-builds:/builds" \
--docker-volumes="gitlab-runner-cache:/cache" \
--docker-volumes="/var/run/docker.sock:/var/run/docker.sock"
```
Run this command in your terminal.
* After a successful runner registration, refresh the page. You will see a specific active runner like this:
![Image of Yaktocat](./registered-specific-runner.png)
* Now, be sure you are in the project root directory. Add the repository of the project which you have created previously. Use the following command:
```bash
git init
git remote add origin http://localhost:8080/root/hello-cloud.git
git add .
git commit -m "Initial commit"
git push -u origin master
```
The project will be pushed to gitlab, runner will be run and two new container will be build.
* To view the runner job, go to "CI/CD"->"Pipelines". You will see something like this:
![Image of Yaktocat](./successfull_login.png)

* When all the job succeded, go back to your terminal and run the follwing command:
```bash
sudo docker ps -a
```
The output will be like this which indicates that all containers (two new containers, service1 and service2) are up.
![Image of Yaktocat](./all-container-up.png)
You can see all the 4 containers are up and running.
* Use the following api to test the application.
### localhost:8001
Output:
```bash
{
    "service1": {
        "remote": "Req came from 192.168.16.1:54512",
        "local": "Req served at 192.168.16.3:8001"
    },
    "service2": {
        "remote": "Req came from 192.168.16.3:38090",
        "local": "Req served at 192.168.16.2:8002"
    }
}
```
### localhost:8001/fibo
Input:
```bash
{
	"number": 10
}
```
Output:
```bash
{
    "fibo": [
        0,
        1,
        1,
        2,
        3,
        5,
        8,
        13,
        21,
        34,
        55
    ]
}
```

### localhost:8001/run-log

Output:
```bash
[
    {
        "service2": "BOOT 2019-12-07 18:48:39"
    },
    {
        "service1": "BOOT 2019-12-07 18:48:40"
    },
    {
        "service1": "SHUTDOWN 2019-12-07 19:3:55"
    },
    {
        "service2": "SHUTDOWN 2019-12-07 19:3:55"
    },
    {
        "service2": "BOOT 2019-12-07 19:14:30"
    },
    {
        "service1": "BOOT 2019-12-07 19:14:31"
    }
]
```
N.B: First time, you will not see the shutdown information. Just two BOOT information of service1 and service 2 will be seen. But, when you run the /shutdown api, git push to the repository again, let the job run first and succeded, and again check /run-log api, you will seee the given output above.

### localhost:8001/shutdown
This api will stop service1 and service2 services and stop both containers.
Output:
![Image of Yaktocat](./2-container-down.png)

You can see service1 and service2 container currently down.